# Setup server tutorial

A tutorial to setup your machine to run multiples projects with different php version in ubuntu 22.04

## 1 - Mysql

Install Mysql and setup secure config

```bash
sudo apt update
sudo apt install mysql-server -y
sudo mysql_secure_installation
sudo mysql -u root -p
```

Update root password

```sql
ALTER USER 'root'@'localhost'IDENTIFIED WITH mysql_native_password BY 'secret';
FLUSH PRIVILEGES;
exit
```

## 2 - Nginx

Install nginx

```bash
sudo apt install nginx -y
```

## 3 - Install PHP

Add repository and install php 7.4 and 8.1, but you can install all php version, just need to reply install command with your php version

```bash
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:ondrej/php
sudo apt update -y
sudo apt install php7.4 php7.4-cli php7.4-fpm -y
sudo apt install php8.1 php8.1-cli php8.1-fpm -y
sudo apt install php7.4-{mysql,curl,xsl,gd,tokenizer,common,xml,zip,xsl,redis,xdebug,soap,bcmath,mbstring,gettext,imagick} -y
sudo apt install php8.1-{mysql,curl,xsl,gd,tokenizer,common,xml,zip,xsl,redis,xdebug,soap,bcmath,mbstring,gettext,imagick} -y
```

## 3.1 - Create php config file

Create <www.conf> to listen a port so we can use this in nginx config file.

Example php 7.4 on port 9074

```bash
sudo printf "[www]\nuser = www-data\ngroup = www-data\nlisten = 9074\nlisten.owner = www-data\nlisten.group = www-data\npm = dynamic\npm.max_children = 5\npm.start_servers = 2\npm.min_spare_servers = 1\npm.max_spare_servers = 3\n" >> /etc/php/7.4/fpm/pool.d/www.conf
```

Example php 8.1 on port 9081

```bash
sudo printf "[www]\nuser = www-data\ngroup = www-data\nlisten = 9081\nlisten.owner = www-data\nlisten.group = www-data\npm = dynamic\npm.max_children = 5\npm.start_servers = 2\npm.min_spare_servers = 1\npm.max_spare_servers = 3\n" >> /etc/php/8.1/fpm/pool.d/www.conf
```

## 3.2 Xdebug

Install xdebug and depedency

```bash
sudo apt-get install php-dev
sudo apt install php-pear
pecl install xdebug
```

Setup xdebug config by php version

php 7.4

```bash
sudo printf "xdebug.remote_enable=1\nxdebug.remote_handler=dbgp\nxdebug.remote_mode=req\nxdebug.remote_host=127.0.0.1\nxdebug.remote_port=9000\n" >> /etc/php/7.4/cli/conf.d/20-xdebug.conf
```

php 8.1

```bash
sudo printf "xdebug.remote_enable=1\nxdebug.remote_handler=dbgp\nxdebug.remote_mode=req\nxdebug.remote_host=127.0.0.1\nxdebug.remote_port=9000\n" >> /etc/php/8.1/cli/conf.d/20-xdebug.conf
```

## 3.3 Composer

Install composer

```bash
curl -sS https://getcomposer.org/installer -o composer-setup.php 
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer 
sudo rm -rf composer-setup.php
```

Example using composer with different php version

php 7.4

```bash
php7.4 /usr/local/bin/composer
```

php 8.2

```bash
php8.2 /usr/local/bin/composer
```

## 5 - Setup Nginx Sites

Create directories to your projects and logs

```bash
mkdir -p /var/www/html/greenn-back
mkdir -p /var/www/html/greenn-gateway
mkdir -p /var/log/nginx/logs
```

### 5.1 - Nginx config examples

Example creating two sites with different php version

#### Example with php 7.4 cause is listening php-fpm in port 9074

```bash
sudo nano /etc/nginx/conf.d/greenn-back.wladi.com.br.conf
```

```conf
server {
    listen 80;
    server_name greenn-back.wladi.com.br;

    root /var/www/html/greenn-back/src/public;
    index index.php index.html index.htm;

    access_log /var/log/nginx/logs/greenn-back_log;
    error_log /var/log/nginx/logs/greenn-back_log error;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        root /var/www/html/greenn-back/src/public;
        fastcgi_pass   127.0.0.1:9074;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
        include /etc/nginx/fastcgi_params;
    }
}
```

#### Example with php 8.1 cause is listening php-fpm in port 9081

```bash
sudo nano /etc/nginx/conf.d/greenn-gateway.wladi.com.br.conf
```

```conf
server {
    listen 80;
    server_name greenn-gateway.wladi.com.br;

    root /var/www/html/greenn-gateway/src/public;
    index index.php index.html index.htm;

    access_log /var/log/nginx/logs/greenn-gateway_log;
    error_log /var/log/nginx/logs/greenn-gateway_log error;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        root /var/www/html/greenn-back/src/public;
        fastcgi_pass   127.0.0.1:9081;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
        include /etc/nginx/fastcgi_params;
    }
}
```

## 6 - Redis

Install redis server

```bash
sudo apt install redis-server -y
sudo systemctl restart redis.service
```

## 7 - RabbitMQ

To use RabbitMQ, you need to install it and upgrade Erlang to the latest version, because RabbitMQ depends on Erlang 25 or higher, and Ubuntu 22.04 only supports up to Erlang 24.

User: admin
Pass: secret

```bash
curl -s <https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.deb.sh> | sudo bash
sudo printf "deb <https://packages.erlang-solutions.com/ubuntu> focal contrib" >> /etc/apt/sources.list.d/erlang.list
wget <https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc> && sudo apt-key add erlang_solutions.asc

sudo rm -rf erlang-solutions_2.0_all.deb  erlang_solutions.asc
sudo apt update
sudo apt install esl-erlang -y
sudo apt install rabbitmq-server -y
sudo systemctl enable rabbitmq-server
sudo rabbitmq-plugins enable rabbitmq_management
sudo rabbitmqctl add_user admin secret
sudo rabbitmqctl set_user_tags admin administrator
sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
```

### Helper debugger commands

```bash
rm -rf /etc/php/8.2/fpm/pool.d/www.conf /etc/php/8.1/fpm/pool.d/www.conf /etc/php/8.0/fpm/pool.d/www.conf /etc/php/7.4/fpm/pool.d/www.conf
sudo systemctl restart php-8.2-fpm
netstat -an | grep :9000
```
